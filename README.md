# Monasca-Log-Schema
This project adds schema for kafka topics

## Available tags
- kafka-log-topics

## Configuration
- zookeeper_hosts (Default: "localhost:2181")
- kafka_log_topics_enabled (Default: true)
- kafka_log_topic_partitions (Default: 4)
- kafka_log_topic_replicas (Default: 3)
- kafka_log_topic_max_message_bytes (Default: 1048576)
- kafka_log_topic_retention_bytes (Default: -1)
- kafka_log_topic_retention_ms (Default: 3600000)

## License

Apache License, Version 2.0

## Author Information

Kamil Choroba
